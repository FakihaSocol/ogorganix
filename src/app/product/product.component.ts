import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router';

const ELEMENT_DATA= [
  {productName: "Eye Love Caffeine", price: 200, category: "Skin Care", productImage: './assets/img/avatar.png'},
  {productName: "Eye Love Caffeine", price: 200, category: "Skin Care", productImage: './assets/img/avatar.png'},
  {productName: "Eye Love Caffeine", price: 200, category: "Skin Care", productImage: './assets/img/avatar.png'},
  {productName: "Eye Love Caffeine", price: 200, category: "Skin Care", productImage: './assets/img/avatar.png'},
 ];
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  displayedColumns: string[] = ['s.no','productName', 'price', 'category', 'productImage','actions'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private route:Router) { }

  ngOnInit() {
   
  }
  editProduct(element){
this.route.navigate(['/edit-product'])
  }
}
