import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { ProductComponent } from '../../product/product.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import { MaterialModule } from '../../app.material.module';
import { CategoriesComponent } from '../../categories/categories.component';
import { ScentsComponent } from '../../scents/scents.component';
import { AddProductComponent } from '../../product/add-product/add-product.component';
import { EditProductComponent } from '../../product/edit-product/edit-product.component';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MaterialModule
  ],
  declarations: [
    ProductComponent,
    CategoriesComponent,
    ScentsComponent,
    AddProductComponent,
    EditProductComponent,
  ]
})

export class AdminLayoutModule {}
