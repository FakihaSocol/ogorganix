import { Routes } from '@angular/router';

import { ProductComponent } from '../../product/product.component';

import { AdminGuard } from '../../../app/shared/guard/admin.guard';
import { EditProductComponent } from 'app/product/edit-product/edit-product.component';
export const AdminLayoutRoutes: Routes = [
    
    { path: 'product',      component: ProductComponent},
    { path: 'edit-product',      component: EditProductComponent},
   
];
