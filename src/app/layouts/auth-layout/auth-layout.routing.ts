import { Routes } from '@angular/router';
import { LoginGuard } from '../../../app/shared/guard/login.guard';
import { LoginComponent } from 'app/login/login.component';

export const AuthLayoutRoutes: Routes = [
  { path: '',          component: LoginComponent},
    { path: 'login',          component: LoginComponent },
];
