
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatMenuModule} from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
    imports:[MatCheckboxModule,MatPaginatorModule,MatRadioModule,MatInputModule, MatFormFieldModule,MatDatepickerModule,MatTableModule,MatCardModule,MatButtonModule,MatGridListModule,MatIconModule,MatTooltipModule,MatMenuModule],
    exports:[MatCheckboxModule,MatPaginatorModule,MatRadioModule,MatInputModule, MatFormFieldModule,MatDatepickerModule,MatTableModule,MatCardModule,MatButtonModule,MatGridListModule,MatIconModule,MatTooltipModule,MatMenuModule]
  })
export class MaterialModule { }