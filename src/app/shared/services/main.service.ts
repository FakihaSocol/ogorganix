import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
// import swal from 'sweetalert';
import { Router } from '@angular/router';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
@Injectable({
  providedIn: 'root'
})
export class MainService {
    apiUrl = environment.apiUrl;
    constructor(private http: HttpClient,private router:Router) { }
  // swalBasic(title, icon, text) {
  //   swal({
  //     title: title,
  //     icon: icon,
  //     text: text,
  //     })
  // }
   private handleError(error:HttpErrorResponse)
  {
    if(error.error instanceof ErrorEvent)
    {
      console.error('An error occurred:', error.error.message);
    }
    else  if(error && error.status === 401 )
    {
      this.logOut();
      }
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
      return throwError(
        'Something bad happened; please try again later.');
  }
  SetAuthToken(token) {
    localStorage.setItem('PrayeR_SloT_SysteM_Auth_AcceSS_TokeN', JSON.stringify(token));

  }
  SetUser(user) {
    localStorage.setItem('PrayeR_SloT_SysteM_User', JSON.stringify(user));

  }
  GetUser() {
    const user = JSON.parse(localStorage.getItem('PrayeR_SloT_SysteM_User'));
    return user;

  }

  GetAuthToken() {
    const token = JSON.parse(localStorage.getItem('PrayeR_SloT_SysteM_Auth_AcceSS_TokeN'));
    return token;
  }
  LoginUser(userobj) {
    const headers = new HttpHeaders({ 'Authorization': 'Basic c29jb2w6dG9wc2VjcmV0' });
    return this.http.post(this.apiUrl + `oauth/token?grant_type=password&username=${userobj.email}&password=${userobj.password}`, userobj, { headers: headers });
  }
  logOut(){
    localStorage.removeItem('PrayeR_SloT_SysteM_Auth_AcceSS_TokeN');
    localStorage.removeItem('PrayeR_SloT_SysteM_User');
    location.reload();
    this.router.navigate(['/login']);
  }
  matchingPassword(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.matchingPassword) {
        return;
      }

      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ matchingPassword: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }
}  