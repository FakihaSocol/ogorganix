import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { MainService } from '../services/main.service';

@Injectable({
    providedIn: 'root'
})
export class LoginGuard implements CanActivate {

    constructor(private router: Router, private _mainService: MainService) {

    }
    canActivate() {
        if (this._mainService.GetAuthToken()) {
            this.router.navigate(['login']);
            return false;
        }
        else {
            return true;
        }
    }

}
